# duplicacy-trickle-checker

This script can be used to validate the integrity of duplicacy backups by restoring a random selection of files while staying
within a specified download limit.

The intended use-case for this script is to validate a random selection of files in your duplicacy repository while staying
under the free daily egress bandwidth limits for Backblaze B2.

# Setup and Usage

This script requires Python 3 to run.

Clone or download the repository:
```
git clone git@gitlab.com:LeerSpace/duplicacy-trickle-checker.git
```

Then configure the `DUPLICACY_DIR`, `DOWNLOAD_LIMIT`, and `LOG_LEVEL` variables towards the beginning
of `trickle-checker.py` as desired.

The `trickle-checker.py` script can then be run using chron (ideally daily) or manually from the command line.