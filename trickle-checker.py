#!/usr/bin/env python3

# Settings
# Specify the path to the duplicacy repo to check
DUPLICACY_DIR = "/path/to/repo"

# Specify the maximum size of files to download in bytes
DOWNLOAD_LIMIT = "524288000" # 500 MiB (until changed)

# output detail, options are DEBUG, INFO, WARNING, ERROR, CRITICAL
LOG_LEVEL = "INFO"

# imports
import os
import sys
import subprocess
import tempfile
import random
import re
import shutil
import logging

# script skeleton:
# - validate inputs
# - check for dependencies
# - create temporary local repo
# - get random snapshot
# - get snapshot file list
# - filter out files that are too large
# - select a random selection of files < max download size
# - clean up temporary repo

def check_inputs(duplicacy_dir, filesize_limit, log_level):
    """Return true if script inputs are valid, false otherwise"""
    if not os.path.isdir(duplicacy_dir):
        logging.error("invalid duplicacy repo path setting")
        return False
    if not filesize_limit.isdigit():
        logging.error("invalid download limit setting")
        return False
    numeric_level = getattr(logging, log_level.upper(), None)
    if not isinstance(numeric_level, int):
        logging.error("invalid logging level setting")
        return False
    return True

def validate_setup(duplicacy_dir):
    """Validate that duplicacy is installed and the repo is setup."""
    # check that duplicacy is installed
    # check that repo is correctly initialized
    pass

def create_temp_repo(duplicacy_dir):
    """Create a temporary repo based on the original to restore to."""
    # create temporary .duplicacy directory and copy over preferences
    temp_dir = tempfile.TemporaryDirectory()
    # Copy relevant files from original .duplicacy directory
    fromDirectory = os.path.join(os.path.sep, duplicacy_dir, ".duplicacy")
    toDirectory = os.path.join(os.path.sep, temp_dir.name, ".duplicacy")
    shutil.copytree(fromDirectory, toDirectory)
    return temp_dir

def get_random_snapshot(temp_dir):
    """Get random snapshot from snapshots in storage."""
    p = re.compile('revision (.*?) created')
    # duplicacy list
    snapshot_list = []
    proc = subprocess.Popen(["duplicacy", "list"], cwd=temp_dir.name, stdout=subprocess.PIPE)
    for line in iter(proc.stdout.readline, ''):
        templine = line.decode('utf-8').rstrip()
        if (templine == ''):
            break
        if "revision" in templine:
            snapshot_list.append(p.findall(templine)[0])
    return random.choice(snapshot_list)

def get_fileline_listing(temp_dir, snapshot, filesize_limit):
    """Get list of file output from duplicacy, filtering out ones larger than the specified limit (in bytes)."""
    # duplicacy list -r <snapshot> -files
    # files larger than filesize_limit shouldn't be included
    file_list = []
    proc = subprocess.Popen(["duplicacy", "list", "-r", snapshot, "-files"], cwd=temp_dir.name, stdout=subprocess.PIPE)
    for line in iter(proc.stdout.readline, ''):
        templine = line.decode('utf-8').rstrip()
        if (templine == ''):
            break
        temp_size = templine.strip().split(' ')[0]
        # Ignore lines that aren't for files
        if not temp_size.isdigit():
            continue
        # Ignore zero byte files and files that exceed the maximum size
        if (int(temp_size) == 0 or int(temp_size) >= filesize_limit):
            continue
        file_list.append(templine.strip())
    return file_list

def get_random_files(fileline_list, total_size_limit):
    """Gets random list of files that fit within download limit (in bytes)."""
    attempts_left = 1000  # maximum failed attempts made to find random files
    running_total = 0  # tracks size of files selected so far
    filename_piece = 4  # location in duplicacy file listing of filename
    random_file_list = []
    while attempts_left > 0 and len(fileline_list) > 0:
        temp_line = random.choice(fileline_list)
        split_line = temp_line.split(' ')
        temp_filesize = int(split_line[0])
        temp_filename = ' '.join(split_line[4:]) # handles cases where filename contains spaces
        #print(str(len(random_file_list)) + ' ' + str(running_total) + ' ' + str(attempts_left), flush=True)
        if running_total + temp_filesize <= total_size_limit:
            random_file_list.append(temp_filename)
            running_total += temp_filesize
            fileline_list.remove(temp_line) # don't select line more than once
        elif running_total == total_size_limit:
            attempts_left = 0
        else:
            attempts_left -= 1
            fileline_list.remove(temp_line) # if this file didn't fit, remove it from the list
    print(running_total, flush=True)
    return random_file_list

def temp_restore(temp_dir, snapshot, file_list):
    """Try to restore the files to validate their integrity."""
    # duplicacy restore -r <snapshot> +"path/to/each/file"
    command_array = ["duplicacy", "restore", "-r", snapshot, "-ignore-owner", "-stats"] + ["+" + file for file in file_list]
    #print(command_array)
    proc = subprocess.Popen(command_array, cwd=temp_dir.name, stdout=subprocess.PIPE)
    (out,err) = proc.communicate()
    for line in out.splitlines():
        templine = line.decode('utf-8').rstrip()
        if (templine == ''):
            break
        logging.info(templine) # informational logging from output command
    # error output based on error proc exit code
    rc = proc.returncode
    if rc > 0:
        logging.error("an error was encountered while trying to restore the file selection")
    else:
        logging.info("files were restored successfully")

def remove_temp_repo(temp_dir):
    """Clean up temporary files."""
    temp_dir.cleanup()


if __name__ == "__main__":
    inputs_valid = check_inputs(DUPLICACY_DIR, DOWNLOAD_LIMIT, LOG_LEVEL)
    if not inputs_valid:
        sys.exit(1)
    
    # reassigning inputs
    numeric_level = getattr(logging, LOG_LEVEL.upper(), None)
    logging.basicConfig(format='%(asctime)s %(message)s', level=numeric_level)
    duplicacy_dir = DUPLICACY_DIR
    download_limit = int(DOWNLOAD_LIMIT)

    logging.debug("original repo: " + duplicacy_dir)
    logging.debug("download limit: " + str(download_limit) + " bytes")

    #validate_setup()

    logging.info("creating temporary repo")
    temp_dir = create_temp_repo(duplicacy_dir)
    logging.debug("created temporary repo: " + temp_dir.name)

    logging.info("getting random snapshot")
    random_snapshot = get_random_snapshot(temp_dir)
    if len(random_snapshot) == 0:
        logging.error("couldn't obtain random snapshot")
        remove_temp_repo(temp_dir)
        sys.exit(duplicacy_dir + " is not a directory.")
    logging.debug("selected snapshot " + random_snapshot)

    logging.info("getting snapshot file listing")
    filtered_fileline_list = get_fileline_listing(temp_dir, random_snapshot, download_limit)
    logging.debug("found " + str(len(filtered_fileline_list)) + " files meeting criteria")
    if len(filtered_fileline_list) == 0:
        logging.error("no files in snapshot that meet criteria")
        sys.exit(1)

    logging.info("picking random files totalling under " + str(download_limit) + " bytes")
    random_files = get_random_files(filtered_fileline_list, download_limit)
    
    logging.info("restoring random selection of files")
    temp_restore(temp_dir, random_snapshot, random_files)
    logging.info("removing temp repo")
    remove_temp_repo(temp_dir)